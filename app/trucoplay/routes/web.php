<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Response;
use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
  return view('home');
});

$router->get('jogos', ['uses' => 'PartidaController@mostrar']);

$router->post('jogos/novo', ['uses' => 'PartidaController@create']);

$router->post('jogos/pontuar/{id}', ['uses' => 'TimeController@pontuar']);

$router->post('jogos/terminar/{id}', ['uses' => 'PartidaController@terminar']);