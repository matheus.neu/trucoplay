<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Time extends Model {
  
  protected $fillable = [
    'jogador1','jogador2','partida','pontos'
  ];

  protected $hidden = [];

}