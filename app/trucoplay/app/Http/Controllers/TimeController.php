<?php

namespace App\Http\Controllers;

use App\Time;
use Illuminate\Http\Request;

class TimeController extends Controller {

  public function pontuar($id, Request $request){
    $time = Time::findOrFail($id);
    $time->update($request->json()->all());

    if($time->save()){
      return response()->json([$time],200);
    }

    return response()->json(['Erro ao salvar os dados!'],500);

  }

}