<?php

namespace App\Http\Controllers;

use App\Partida;
use App\Time;
use Illuminate\Http\Request;

class PartidaController extends Controller {

  public function create(Request $request){
    $partida = Partida::create([]);
    $dados = $request->json()->all();

    $time1 = Time::create([
      'jogador1' => $dados['jogador1'],
      'jogador2' => empty($dados['jogador2'])? null : $dados['jogador2'],
      'partida' => $partida->id,
      'pontos' => 0
    ]);
    
    $time2 = Time::create([
      'jogador1' => $dados['jogador3'],
      'jogador2' => empty($dados['jogador4'])? null : $dados['jogador4'],
      'partida' => $partida->id,
      'pontos' => 0
    ]);

    if($partida->save() && $time1->save() && $time2->save()){
      return response()->json([$partida, $time1, $time2],200);
    }

    return response()->json(['Não foi possivel gravar os dados!'],500);
  }

  public function terminar($id, Request $request){
    $partida = Partida::findOrFail($id);
    $partida->update($request->json()->all());

    if($partida->save()){
      return response()->json([$partida],200);
    }

    return response()->json(['Erro ao salvar os dados!'],500);
  }

  public function mostrar(){
    /*
      IMPORTANTE:
      Coluna times contém os dois times
      Coluna jogadores1 terá o campo jogador1 dos DOIS times, separados por virgula,
      assim como a coluna jogadores2
    */
    $jogos = app('db')->select(
      "select times.partida, group_concat(times.id) times,
        group_concat(jogador1) jogadores1, 
        group_concat(jogador2) jogadores2, 
        group_concat(times.pontos) pontos,
        partidas.time_vencedor
     from times
     inner join partidas on (partidas.id = times.partida)
     group by times.partida");

    return view('jogos', ['jogos' => $jogos]);
  }

}