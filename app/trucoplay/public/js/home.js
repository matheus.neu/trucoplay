var jogo = {
  partida: null,
  limite: null,
  time1: {
    id: null,
    jogador1: null,
    jogador2: null,
    pontos: 0,
  },
  time2: {
    id: null,
    jogador1: null,
    jogador2: null,
    pontos: 0,
  }
};

$(document).ready(function(){

  $(".navbar-brand, .nav-link").click(function(){
    if(jogo.partida != null){
      if(!confirm("Voce iniciou um jogo, se sair agora ele não poderá ser retomado!")){
        return false;
      }
    }
  });

  $("#formulario").submit(function(){
    jogo.limite = parseInt($("#limite").val());
    jogo.time1.jogador1 = $("#jogador1").val();
    jogo.time1.jogador2 = $("#jogador2").val();
    jogo.time2.jogador1 = $("#jogador3").val();
    jogo.time2.jogador2 = $("#jogador4").val();

    novo_jogo({
      jogador1: $("#jogador1").val(),
      jogador2: $("#jogador2").val(),
      jogador3: $("#jogador3").val(),
      jogador4: $("#jogador4").val()
    });
    return false;
  });

  $(".pontuar").click(function(){
    $(".pontuar").attr("disabled","true");
    var time = $(this).val();
    if(time == 1){
      jogo.time1.pontos++;
      pontuar(jogo.time1, 1);
    }else{
      jogo.time2.pontos++;
      pontuar(jogo.time2, 2);
    }
  });

});

function pontuar(data, time){
  $.ajax({
    url: "jogos/pontuar/" + data.id,
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify(data),
    success: function(newdata) {
      $("#pontosTime" + time).text(newdata[0].pontos);
      if(newdata[0].pontos == jogo.limite){
        terminarJogo({
          id: jogo.partida,
          time_vencedor: newdata[0].id
        }, time);
      }
      $(".pontuar").removeAttr("disabled");
    },
    error: function (request, status, error) {
      alert(request.responseJSON[0]);
      console.log(request);
    },
    cache: false,
    contentType: false,
    processData: false,
    xhr: function() {
        var myXhr = $.ajaxSettings.xhr();
        return myXhr;
    }
  });
}

function novo_jogo(data){
  $.ajax({
    url: "jogos/novo",
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify(data),
    success: function(newdata) {
      jogo.partida = newdata[0]['id'];
      jogo.time1.id = newdata[1]['id'];
      jogo.time2.id = newdata[2]['id'];

      $("#formulario").hide();
      $("#jogo").show();
    },
    error: function (request, status, error) {
      alert(request.responseJSON[0]);
      console.log(request);
    },
    cache: false,
    contentType: false,
    processData: false,
    xhr: function() {
        var myXhr = $.ajaxSettings.xhr();
        return myXhr;
    }
  });
}

function terminarJogo(data, time){
  $.ajax({
    url: "jogos/terminar/" + data.id,
    type: 'POST',
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify(data),
    success: function(newdata) {
      alert("O jogo terminou e o vencedor foi o time " + time);
      location.reload();
    },
    error: function (request, status, error) {
      alert(request.responseJSON[0]);
      console.log(request);
    },
    cache: false,
    contentType: false,
    processData: false,
    xhr: function() {
        var myXhr = $.ajaxSettings.xhr();
        return myXhr;
    }
  });
}