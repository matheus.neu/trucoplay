<!doctype html>
<html lang="pt-br">
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Site para gerenciamento de Jogos de truco">
		<title>Home</title>
    <link rel="shortcut icon" href="/trucoplay/public/img/icone.png">
		
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/app.css" rel="stylesheet" type="text/css">
    
    <style>
      #jogo{
        display:none;
      }
      @media(max-width: 767px){
        .margin-bottom15{
          margin-bottom: 15px;
        }
      }
    </style>
	</head>	
	<body>
  <body>
    
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	    <a class="navbar-brand" href="">TrucoPlay</a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item">
		       	<a class="nav-link" href="/trucoplay/public/jogos">Jogos</a>
		      </li>
	    	</ul>
	    </div>
	  </nav>

		<div class="container" style="padding-top: 65px; ">
		  <form id="formulario">
		  	<h3 class="text-center">Time 1</h3>
		  	<div class="form-group row">
			    <label class="col-form-label col-lg-4 col-md-4 col-sm-12 text-center">Jogador 1</label>
		  		<div class="col-lg-6 col-md-6 col-sm-12">
		  		  <input id="jogador1" data-id="1" placeholder="Nome do jogador" type="text" required class="form-control"/>
		  		</div>
		  	</div>
		  	<div class="form-group row">
			    <label class="col-form-label col-lg-4 col-md-4 col-sm-12 text-center">Jogador 2</label>
			  	<div class="col-lg-6 col-md-6 col-sm-12">
			  	  <input id="jogador2" data-id="1" placeholder="Nome do jogador (Opcional)" type="text" class="form-control"/>
		  		</div>
		  	</div>

        <h3 class="text-center">Time 2</h3>
			  <div class="form-group row">
			    <label class="col-form-label col-lg-4 col-md-4 col-sm-12 text-center">Jogador 1</label>
			  	<div class="col-lg-6 col-md-6 col-sm-12">
			  	  <input id="jogador3" data-id="2" placeholder="Nome do jogador" type="text" required class="form-control"/>
		  		</div>
		  	</div>

		  	<div class="form-group row">
			    <label class="col-form-label col-lg-4 col-md-4 col-sm-12 text-center">Jogador 2</label>
			  	<div class="col-lg-6 col-md-6 col-sm-12">
			  	  <input id="jogador4" data-id="2" placeholder="Nome do jogador (Opcional)" type="text" class="form-control"/>
			   	</div>
			  </div>

        <div class="form-group row">
			    <label class="col-form-label col-lg-4 col-md-4 col-sm-12 text-center">Pontuação Limite</label>
			  	<div class="col-lg-6 col-md-6 col-sm-12">
            <select required id="limite" class="form-control">
              <option selected disabled value="">Selecione uma opção!</option>
              <option value="12">12 Pontos</option>
              <option value="24">24 Pontos</option>
            </select>
			   	</div>
			  </div>

        <div class="form-group text-center">
          <button class="btn btn-success" type="submit">Jogar</button>
        </div>

			</form>

      <div id="jogo">
        <div class="form-group text-center">
          <h3></h3>
        </div>
        <div class="form-group row">
          <div class="col-lg-6 col-md-6 col-sm-12 text-center margin-bottom15">
            <b id="pontosTime1">0</b>
            <br>
            <button class="btn btn-info pontuar" value="1">Pontuar Time 1</button>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 text-center">
            <b id="pontosTime2">0</b>
            <br>
            <button class="btn btn-info pontuar" value="2">Pontuar Time 2</button>
          </div>
        </div>
      </div>

		</div><!-- /.container -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/home.js"></script>
	</body>
</html>
