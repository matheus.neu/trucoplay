<!doctype html>
<html lang="pt-br">
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Site para gerenciamento de Jogos de truco">
		<title>Jogos</title>
    <link rel="shortcut icon" href="/trucoplay/public/img/icone.png">
		
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/app.css" rel="stylesheet" type="text/css">
	</head>	
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	    <a class="navbar-brand" href="">TrucoPlay</a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item">
		       	<a class="nav-link" href="/trucoplay/public/">Novo Jogo</a>
		      </li>
	    	</ul>
	    </div>
	  </nav>

		<div class="container" style="padding-top: 65px; ">
      <?php  if(empty($jogos)): ?>
        <div class="text-center"><h5>Nenhum jogo cadastrado!</h5></div>
        <div class="text-center">
          <img id="img_no_game" src="/trucoplay/public/img/icone.png" />
        </div>
      <?php endif; ?>
      <?php foreach($jogos as $jogo): ?>
        <div class="card">
          <div class="card-header">
            Partida <?= $jogo->partida ?>
          </div>
          <div class="card-body">
            <h5 class="card-title">Vencedor: <?= empty($jogo->time_vencedor)? 'Nenhum':$jogo->time_vencedor ?></h5>
            <p class="card-text">
              <span class="margeado">Time: <?= explode(',',$jogo->times)[0] ?></span>
              <span class="margeado">Jogador 1: <?= explode(',',$jogo->jogadores1)[0] ?></span>
              <span class="margeado">Jogador 2: <?= (strpos($jogo->jogadores2,',') !== false)? explode(',',$jogo->jogadores2)[0] : '' ?></span>
              <span>Pontos: <?= explode(',',$jogo->pontos)[0] ?></span>
              <br>
              
              <span class="margeado">Time: <?= explode(',',$jogo->times)[1] ?></span>
              <span class="margeado">Jogador 1: <?= explode(',',$jogo->jogadores1)[1] ?></span>
              <span class="margeado">Jogador 2: <?= (strpos($jogo->jogadores2,',') !== false)? explode(',',$jogo->jogadores2)[1] : '' ?></span>
              <span>Pontos: <?= explode(',',$jogo->pontos)[1] ?></span>
            </p>
          </div>
        </div>
        <br>
      <?php endforeach; ?>
		</div><!-- /.container -->

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
	</body>
</html>